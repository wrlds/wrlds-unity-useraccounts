﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static WRLDSUtils; 

public class UserAccounts : Singleton<WRLDSBallPlugin>
{
    public Text contactText, userText, deviceAddressText, cloudBounceCountText, bounceCountText, requestPasswordChangeText, leaderboardListText;
    public Text leaderNickNameText, secondNickNameText, thirdNickNameText, contenderNickNamesText, userBouncesText;
    public Text leaderScoreText, secondScoreText, thirdScoreText, contenderScoresText;

    private bool connected = false, retrievedData = false;
    private string contact, user, deviceAddress, password, mail, screen, previousScreen, requestPasswordChange, leaderboardList;
    private string leaderNickName, secondNickName, thirdNickName, contenderNickNames, contenderScores;

    private int bounceCount, cloudBounceCount, resumeTriggered = 0;
    private int leaderScore, secondScore, thirdScore, userBounces;
    public Button[] goToLogin;
    public Button signUpButton, signInButton, continueAsGuestButton, signOutButton, resendConfirmationLinkButton, scanButton, goToUserData, goToAccountButton, goToLeaderboardButton, addBallToAccountButton, changeMailButton, changePasswordButton, forgotPasswordButton, requestNewPasswordButton, submitNewPasswordButton, submitNewMailButton, cancelPasswordRequest, cancelPasswordReset, deleteUserAccountButton, changeNickNameButton, exitNickNameInputButton;
    public InputField passwordInput, mailInput, oldPasswordInput, newPasswordInput, newMailInput, resetPasswordCodeInput, newPasswordAfterResetInput, resetMailCodeInput, nickNameInput;
    public GameObject loginScreen, userScreen, accountScreen, requestResetPasswordScreen, resetPasswordScreen, resetMailScreen, leaderboardScreen;

    private BluetoothState state = 0;

    private void Awake()
    {
        WRLDS.Init();
    }
    // Use this for initialization
    void Start() {
#if UNITY_ANDROID
        WRLDS.GetBounceEvents(true, BounceHandler);
        WRLDS.StartConnectionStateEvents(ConnectionStateHandler);
        //WRLDS.GetGenericCallbacks(true, GenericHandler);

        foreach (var button in goToLogin) {
            button.onClick.AddListener(onGoToLoginClick);
        }

        scanButton.onClick.AddListener(onScanClick);
        signUpButton.onClick.AddListener(onSignUpClick);
        signInButton.onClick.AddListener(onSignInClick);
        continueAsGuestButton.onClick.AddListener(onContinueAsGuestClick);
        signOutButton.onClick.AddListener(onSignOutClick);
        resendConfirmationLinkButton.onClick.AddListener(onResendConfirmationLinkClick);
        addBallToAccountButton.onClick.AddListener(onAddBallToAccountClick);
        goToAccountButton.onClick.AddListener(onGoToAccountClick);
        changeMailButton.onClick.AddListener(onChangeMailClick);
        changePasswordButton.onClick.AddListener(onChangePasswordClick);
        forgotPasswordButton.onClick.AddListener(onForgotPasswordClick);
        requestNewPasswordButton.onClick.AddListener(onRequestNewPasswordClick);
        submitNewMailButton.onClick.AddListener(onSubmitNewMailClick);
        submitNewPasswordButton.onClick.AddListener(onSubmitNewPasswordClick);
        goToUserData.onClick.AddListener(onCancelClick);
        cancelPasswordRequest.onClick.AddListener(onCancelClick);
        cancelPasswordReset.onClick.AddListener(onCancelClick);
        deleteUserAccountButton.onClick.AddListener(onDeleteUserAccountClick);
        goToLeaderboardButton.onClick.AddListener(goToLeaderboardClick);
        changeNickNameButton.onClick.AddListener(onChangeNickNameClick);
        exitNickNameInputButton.onClick.AddListener(onExitNickNameClick);

        addBallToAccountButton.interactable = false;

        nickNameInput.gameObject.SetActive(false);
        changeNickNameButton.gameObject.SetActive(true);
        exitNickNameInputButton.gameObject.SetActive(false);

        screen = "loginScreen";
        previousScreen = "";
        requestPasswordChange = "No user logged in";

        //Retrieves the leaderboard data. The request is asynchronous because the data is fetched from the network. 
        //If the event "leaderboardRetrieved" is true then it you can call WRLDS.GetLeaderboard()
        //WRLDS.FetchBounceLeaderboard();
#endif
    }

    // Update is called once per frame
    void Update () {
#if UNITY_ANDROID
        contactText.text = contact;
        userText.text = user;
        deviceAddressText.text = deviceAddress;
        cloudBounceCountText.text = cloudBounceCount.ToString();
        bounceCountText.text = bounceCount.ToString();
        requestPasswordChangeText.text = requestPasswordChange;
        leaderboardListText.text = leaderboardList;
        leaderNickNameText.text = leaderNickName;
        secondNickNameText.text = secondNickName;
        thirdNickNameText.text = thirdNickName;
        contenderNickNamesText.text = contenderNickNames;
        contenderScoresText.text = contenderScores;

        leaderScoreText.text = leaderScore.ToString();
        secondScoreText.text = secondScore.ToString();
        thirdScoreText.text = thirdScore.ToString();
        userBouncesText.text = userBounces.ToString();

        if (deviceAddress != null) {
            addBallToAccountButton.interactable = true;
        }
        if (screen != previousScreen)  {
            ScreenSelector(screen);
        }
        previousScreen = screen;
#endif
    }

#if UNITY_ANDROID

    void ScreenSelector(string screen) {
        GameObject[] panels = { loginScreen, userScreen, accountScreen, requestResetPasswordScreen, resetPasswordScreen, resetMailScreen, leaderboardScreen };
        string[]  panelStrings = { "loginScreen", "userScreen", "accountScreen", "requestResetPasswordScreen", "resetPasswordScreen", "resetMailScreen", "leaderboardScreen" };
        for (int i = 0; i < panels.Length; i++) {
            if (screen == panelStrings[i]) { panels[i].SetActive(true); }
            else { panels[i].SetActive(false); }
        }
    }

    void BounceHandler(BluetoothDevice device, int type, float sumG, int intensity) {
        connected = true;
        bounceCount++;
    }
    
    void onScanClick() {
        //WRLDS.ReconnectBall();
        WRLDS.ScanForDevices();
    }

    void onSignUpClick() {
        password = passwordInput.text;
        mail = mailInput.text;
        WRLDS.SignUp(mail, password, (bool status, string message) =>
        {     
            if (status == true)
                Debug.Log("SignUp successfull: " + message);

            if (status == false)
                Debug.LogError("SignUp failed: " + message);
        });
    }

    void onSignInClick() {
        mail = mailInput.text;
        password = passwordInput.text;

        WRLDS.SignIn(mail, password, (bool status, string message) =>
        {
            if (status == true)
            {
                screen = "userScreen";
                Debug.Log("SignIn successfull: " + message);
            }
            if (status == false)
            {
                Debug.LogError("SignIn failed: " + message);
                if (message.Contains("PasswordResetRequiredException"))
                {
                    screen = "requestResetPasswordScreen";
                }
            }
        });
    }

    void onContinueAsGuestClick() {
        screen = "userScreen";
    }

    void onSignOutClick() {
        WRLDS.SignOut((bool status, string message) =>
        {
            if (status == true)
            {
                user = "";
                deviceAddress = "";
                cloudBounceCount = 0;
                screen = "loginScreen";

                Debug.Log("SignOut successfull: " + message);
            }

            if (status == false)
                Debug.LogError("SignOut failed: " + message);
        });
    }

    void postUserData(Dictionary<string, string> userData)
    {
        WRLDS.PostUserDataToDatabase(userData, (bool state, string jsonData) => {
            Debug.Log("Post UserData: " + " Values: " + jsonData);
        });
    }

    void onResendConfirmationLinkClick()
    {
        WRLDS.ResendConfirmationLink(mail, (bool status, string message) =>
        {
            if (status == true)
                Debug.Log("ResendConfirmationLink successfull: " + message);

            if (status == false)
                Debug.LogError("ResendConfirmationLink failed: " + message);
        });
    }

    void checkLoginState() {
        bool userLoggedIn = WRLDS.CheckLoginState();
    }

    void getCurrentUser()
    {
        string currentUser = WRLDS.GetCurrentUser();
    }

    void onChangePasswordClick() {
        string oldPassword = oldPasswordInput.text;
        string newPassword = newPasswordInput.text;

        WRLDS.ChangePassword(oldPassword, newPassword, (bool status, string message) =>
        {
            if (status == true)
            {
                Debug.Log("ChangePassword successfull: " + message);
                screen = "userScreen";
            }

            if (status == false)
                Debug.LogError("ChangePassword failed: " + message);
        });
    }

    void onForgotPasswordClick() {
        mail = mailInput.text;
        requestPasswordChange = "For user: " + mail;
        screen = "requestResetPasswordScreen";
    }

    void onRequestNewPasswordClick() {
        screen = "resetPasswordScreen";
        WRLDS.RequestNewPassword(mail, (bool status, string message) =>
        {
            if (status == true)
                Debug.Log("RequestNewPassword successfull: " + message);

            if (status == false)
                Debug.LogError("RequestNewPassword failed: " + message);
        });
    }

    void onSubmitNewPasswordClick()  {
        string resetPasswordCode = resetPasswordCodeInput.text;
        string newPasswordAfterReset = newPasswordAfterResetInput.text;

        WRLDS.ResetPassword(newPasswordAfterReset, resetPasswordCode, (bool status, string message) =>
        {
            if (status == true)
            {
                Debug.Log("ResetPassword successfull: " + message);
                screen = "userScreen";
            }

            if (status == false)
                Debug.LogError("ResetPassword failed: " + message);
        });
    }

    void onSubmitNewMailClick()
    {
        WRLDS.SubmitMailCode("email", resetMailCodeInput.text, (bool status, string message) =>
        {
            if (status == true)
            {
                Debug.Log("SubmitMailCode successfull: " + message);
                screen = "userScreen";
            }

            if (status == false)
                Debug.LogError("SubmitMailCode failed: " + message);
        });
    }

    void onChangeMailClick()
    {
        WRLDS.ChangeAttribute("email", newMailInput.text, (bool status, string message) => {
            if (status == true)
            {
                Debug.Log("ChangeAttribute successfull: " + message);
                screen = "resetMailScreen";
            }

            if (status == false)
                Debug.LogError("ChangeAttribute failed: " + message);
        });
    }
    void onAddBallToAccountClick()
    {
        WRLDS.ChangeAttribute("link_ball", deviceAddress, (bool status, string message) => {
            if (status == true)
                Debug.Log("AddBallToAccount successfull: " + message);

            if (status == false)
                Debug.LogError("AddBallToAccount failed: " + message);
        });
    }

    void onDeleteUserAccountClick() {
        WRLDS.DeleteUserAccount(user, (bool status, string message) => {
            if (status == true)
            {
                user = "";
                deviceAddress = "";
                cloudBounceCount = 0;
                screen = "loginScreen";

                Debug.Log("DeleteUserAccount successfull: " + message);
            }

            if (status == false)
                Debug.LogError("DeleteUserAccount failed: " + message);
        });
    }

    void goToLeaderboardClick()
    {
        screen = "leaderboardScreen";
    }

    void onChangeNickNameClick()
    {
        nickNameInput.gameObject.SetActive(true);
        exitNickNameInputButton.gameObject.SetActive(true);
    }

    void onExitNickNameClick ()
    {
        nickNameInput.gameObject.SetActive(false);
        exitNickNameInputButton.gameObject.SetActive(false);

        WRLDS.ChangeAttribute("nickname", nickNameInput.text, (bool status, string message) => {
            Debug.Log("ChangeAttribute-> Status: " + status + " Message: " + message);
        });
    }


    void onGoToLoginClick() { screen = "loginScreen"; }

    void onGoToAccountClick(){ screen = "accountScreen"; }

    void onCancelClick(){ screen = "userScreen"; }

    void ConnectionStateHandler(BluetoothDevice device, int connectionState) 
    {
        if (connectionState == -1) state = BluetoothState.LINK_LOSS;
        else if (connectionState == 0) state = BluetoothState.DISCONNECTED;
        else if (connectionState == 1) state = BluetoothState.CONNECTED;
        else if (connectionState == 2) state = BluetoothState.CONNECTING;
        else if (connectionState == 3) state = BluetoothState.DISCONNECTING;

        Debug.Log("ConnectionState: " + state);
    }

    void GenericHandler(string action, bool status, string message, AndroidJavaObject result) {
        switch (action) {
            case "signUp":

                break;
            case "signIn":
                
                break;
            case "signOut":

                break;
            case "resendConfirmationLink":

                break;

            //Should be returned directly from SDK rather than with a callback
            case "loginState":

            //Handler for when user data has been fetched from the cloud
            case "userDataReceived":
                //*dataToRetrieve: Define the data that you want to retrieve
                string[] dataToRetrieve = { "email", "nickName", "deviceAddress", "bounceCount"};

                //*userData: Retrieve the data by calling the plugin and setting the return value to a string array
                string[] userData = WRLDS.GetUserData(dataToRetrieve);

                //*totalBounceCount: All the bounces this user has done across all WRLDS apps 
                int totalBounceCount = WRLDS.GetTotalBounceCount();

                //*! Check and parse the response. 
                if (userData != null)
                {
                    //GetUserData can return empty strings if the value could not be retrieved.i.e. { "johnDoe", "", "645}

                    if (userData[0] != "") contact = userData[0];
                    if (userData[1] != "")
                    {
                        user = userData[1];
                        requestPasswordChange = "For user: " + userData[1];
                    }
                    if (userData[2] != "") deviceAddress = userData[2];
                    if (int.TryParse(userData[3], out int parseResult))
                    {
                        cloudBounceCount = parseResult;
                        userBounces = parseResult;
                    }
                }
            
                break;
            case "leaderboardReceived":
                if (status == true)
                {
                    WRLDSUtils.BounceLeaderboard leaderboard = WRLDS.GetLeaderboard();
                    List<BounceCountLeaderboard> bounceCountLeaderboard = leaderboard.bounceCountLeaderboard;
                    for (var i = 0; i < bounceCountLeaderboard.Count; i++)
                    {
                        if (i == 0)
                        {
                            leaderNickName = bounceCountLeaderboard[i].nickName;
                            leaderScore = bounceCountLeaderboard[i].bounceCount;
                        }
                        if (i == 1)
                        {
                            secondNickName = bounceCountLeaderboard[i].nickName;
                            secondScore = bounceCountLeaderboard[i].bounceCount;
                        }
                        if (i == 2)
                        {
                            thirdNickName = bounceCountLeaderboard[i].nickName;
                            thirdScore = bounceCountLeaderboard[i].bounceCount;
                        }
                        else if (i != 0 && i != 1 && i != 2)
                        {
                            contenderNickNames += bounceCountLeaderboard[i].nickName + "\n";
                            contenderScores += bounceCountLeaderboard[i].bounceCount.ToString() + "\n";
                        }
                    }

                    //foreach (var player in bounceCountLeaderboard)
                    //{
                    //    leaderboardList += player.nickName + ":\t  " + player.bounceCount + "\n";
                    //}

                    //foreach (var player in leaderboard.bounceForceLeaderboard)
                    //{
                    //    Debug.Log("player: " + player.nickName + " highest average force: " + player.bounceForce);
                    //}
                }
                if (status == false)
                {
                    Debug.LogError(message);
                }                    
                break;
            case "changePassword":

            case "requestNewPassword":
                 
            case "changeAttributes":

            case "verifyAttribute":

            case "confirmAttribute":

            case "resetPassword":

            case "linkBallToAccount":

            case "deleteUserAccount":

            default:
                break;
        }
    }

    private void OnDestroy()
    {
        WRLDS.StopConnectionStateEvents(ConnectionStateHandler);
    }

#endif
}
